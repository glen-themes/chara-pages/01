// WHY
var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

// replace flaticon names to lowercase
// to match the ones in the stylesheet
document.addEventListener("DOMContentLoaded",() => {
    uiconsLC()
});

function uiconsLC(){
    document.querySelectorAll("i.fi").forEach(fyra => {
        var fyracas = fyra.getAttribute("class").split(" ").pop();
        fyral = fyracas.toLowerCase();
        fyra.classList.remove(fyracas);
        fyra.classList.add(fyral)
    });
}

$(document).ready(function(){
    $(".tumblr_preview_marker___").remove();

    // check jquery version
    var jqver = jQuery.fn.jquery;
    var ver = jqver.replaceAll(".","");


    $("[large-floating-title]").each(function(){
        $(this).wrapInner("<div>")
        $(this).find("div").eq(0).each(function(){
            var tlt = $(this).html();
            $(this).html($.trim(tlt))
        })

        $(this).prependTo(".character-list").addClass("large-floating-title").removeAttr("large-floating-title");

        if(customize_page){
            setTimeout(function(){
                tt_t();
            },699)
        }

        if(on_main){
            setTimeout(function(){
                tt_t();
            },420)
        }

        $(window).load(function(){
            tt_t();
        })

        var force_resize;
        if (typeof (Event) === 'function') {
            force_resize = new Event('resize');
        } else { /*IE*/
            force_resize = document.createEvent('Event');
            force_resize.initEvent('resize', true, true);
        }

        window.dispatchEvent(force_resize);

        document.addEventListener("DOMContentLoaded", () => {
            tt_t()
        })

        $(window).resize(function(){
            tt_t()
        })

        function tt_t(){
            var gethz = $(".character-list").height();
            var botz = "calc(var(--Bottom-Floor-Title-Size) + (var(--Bottom-Floor-Title-Padding) * 2) + " + gethz + "px + var(--Large-Floating-Title-Bottom-Gap))";
            $(".large-floating-title").each(function(){
                $(this).insertBefore(".character-list");
                $(this).css("margin-bottom",botz);
                $(this).css("opacity","1")
            })
        }

    })

    /*---- TUMBLR CONTROLS BS ----*/
    var sike = getComputedStyle(document.documentElement)
              .getPropertyValue("--TumblrControls-Color");

    sike = $.trim(sike);
    if(on_main){
        $(window).load(function(){
            $("iframe#tumblr_controls, .iframe-controls--desktop, .iframe-controls-container").addClass("abeer");

            if(sike == "black"){
                $("iframe#tumblr_controls, .iframe-controls--desktop, .iframe-controls-container").addClass("tc-inv")
            }
        })
    }

    // chad
    if(!$(".cows-ring-endlessly-in-tandem").length){
        if(!$(".navlinks a[href*='glenthemes.tumblr.com']").length){
            if($(".navlinks").length){
                $(".navlinks").append("<a href='//glenthemes.tumblr.com' title='&#10094;&#10094; Fortitude &#10095;&#10095;&#8194;by glenthemes'><i class='fi fi-rr-copyright'></i></a>")
            } else {
                $("body").append("<a class=\'bobroche\' href=\'//glenthemes.tumblr.com\' title=\'&#34;Fortitude&#34; by glenthemes\'></a>");
                $(".bobroche").css({
                    "position":"fixed",
                    "top":"0",
                    "left":"0",
                    "width":"0",
                    "height":"0",
                    "border-style":"solid",
                    "border-width":"14px 14px 0 0",
                    "border-color":"var(--Navlinks-Background) transparent transparent transparent"
                })
            }
        }
    }

    /*---- LOAD IN THE POPUP BOX SKELETON ----*/
    $(".character-list").after("<div class='shrimp'></div>");

    $(".shrimp").prepend('<div class="ovcnt"><div class="maisnon"><div class="popup-box"><a class="dis-close" title="back to characters"><i class="fi fi-rr-Cross"></i></a></div></div></div>').children().unwrap();
    uiconsLC();
    dismiss_ok();

    /*---- MENU CHARACTERS (HORIZONTAL) ----*/

    var boxInOut = getComputedStyle(document.documentElement).getPropertyValue("--Popup-Fade-Speed-RAW");

    $("[menu-character]").each(function(){
        var gen_img = $(this).find("[image-url]").attr("image-url");
        var gen_wdt = $(this).attr("width");
        var gen_tt = $(this).find("[hover-text]").text();

        /*--------------------------*/

        $(this).find("[move-up]").each(function(){
            var mvup = $(this).attr("move-up");
            $(this).parents("[menu-character]").attr("move-up",mvup)
        })

        $(this).find("[move-right]").each(function(){
            var mvright = $(this).attr("move-right");
            $(this).parents("[menu-character]").attr("move-right",mvright)
        })

        $(this).find("[move-down]").each(function(){
            var mvdown = $(this).attr("move-down");
            $(this).parents("[menu-character]").attr("move-down",mvdown)
        })

        $(this).find("[move-left]").each(function(){
            var mvleft = $(this).attr("move-left");
            $(this).parents("[menu-character]").attr("move-left",mvleft)
        })

        /*--------------------------*/

        $(this).attr("title",gen_tt);
        $(this).append("<img src='" + gen_img + "' width='" + gen_wdt + "'>");

        $(this).removeAttr("width");
        $(this).find("[image-url]").remove();
        $(this).find("[hover-text]").remove();

        /*--------------------------*/
        if($(this).is("[move-up]")){
            var mvup = $(this).attr("move-up");
            $(this).find("img").css("margin-bottom",mvup);
        }

        if($(this).is("[move-right]")){
            var mvright = $(this).attr("move-right");
            $(this).find("img").css("margin-left",mvright);
        }

        if($(this).is("[move-down]")){
            var mvdown = $(this).attr("move-down");
            $(this).find("img").css("margin-bottom","calc(0px - " + mvdown + ")");
        }

        if($(this).is("[move-left]")){
            var mvleft = $(this).attr("move-left");
            $(this).css("margin-left","calc(0px - " + mvleft + " + (var(--Character-Image-Spacing) / 2))");
        }

        $(this).click(function(){
            var char_id = $(this).attr("chara-id");
            var ppbox = $(".popup-box");

            var char_tgt = $("[character-box]").filter("[chara-id='" + char_id + "']");

            if(char_tgt.length){
                $(".ovcnt").fadeIn(boxInOut);
            }

            if(char_tgt.is("[box-width]")){
                var bww = char_tgt.attr("box-width");
                ppbox.width(bww)
            }

            if(char_tgt.is("[box-height]")){
                var bhh = char_tgt.attr("box-height");
                ppbox.height(bhh)

                var iacxv = "calc(" + bhh + " - var(--Popup-Title-Padding) - var(--Popup-Padding))";
                $("[popup-img-cont], .mt-evst").height(iacxv)
                $("[popup-img-cont] img").height(iacxv);
            }

            var bisbose = ppbox.find(".dis-close").prop("outerHTML");
            ppbox.empty().prepend(bisbose).append(char_tgt.html());
            dismiss_ok();

            ppbox.find("[image-url]").each(function(){
                $(this).attr("src",$(this).attr("image-url"))
            })
        })
    })

    $("[character-box]").contents().filter(function(){
        return this.nodeType === 3  && this.data.trim().length > 0
    }).wrap("<span/>")

    $("[character-box] [popup-title]").each(function(){
        $(this).nextAll().wrapAll("<div class='maain'>")
    })

    $("[character-box] [image-url]").each(function(){
        $(this).wrap("<div popup-img-cont>");
        var eee = $(this).attr("image-url");
        var goopy = new Image();
        goopy.src = eee;

        if($(this).is("[width]")){
            var urrca = $(this).attr("width");
            $(this).css('width',urrca)
        }

        if($(this).is("[move-up]")){
            var fmkvv = $(this).attr("move-up");
            $(this).css("margin-top","calc(0px - " + fmkvv + ")")
        }

        if($(this).is("[move-right]")){
            var iisne = $(this).attr("move-right");
            $(this).css("margin-left",iisne)
        }

        if($(this).is("[move-down]")){
            var amtgy = $(this).attr("move-down");
            $(this).css("margin-top",amtgy)
        }

        if($(this).is("[move-left]")){
            var ogxlc = $(this).attr("move-left");
            $(this).css("margin-left","calc(0px - " + ogxlc + ")")
        }
    })

    $("[character-box] [popup-img-cont]").each(function(){
        $(this).nextAll().wrapAll("<div class='mt-evst'>")
    })

    // bullet stat rows
    var ic_row = $("[icon-row]");

    for(var soda=0; soda<ic_row.length;){
        if(ver < "180"){
           soda += ic_row.eq(soda).nextUntil(':not([icon-row])').andSelf().wrapAll('<div class="ic-row-cont">').length;
        } else {
           soda += ic_row.eq(soda).nextUntil(':not([icon-row])').addBack().wrapAll('<div class="ic-row-cont">').length;
        }
    }

    // meter bars
    $("[meter-bars] [bar-name]").each(function(){
        $(this).wrap("<div bar-table-row>");
    })

    $("[bar-table-row]").each(function(){
        var barname = $(this).find("[bar-name]").attr("bar-name");
        $(this).prepend("<div class='bar-label'>" + barname + "</div>");
        $(this).append("<div class='bar-cont'>")
    })

    $(".bar-cont").each(function(){
        var pacent = $(this).siblings("[fill]").attr("fill");
        $(this).wrap("<div bar-cont-cell>");
        $(this).append("<div class='bar-fill' style='width:" + pacent + "'></div>")
    })

    $("[meter-bars] [bar-name]").remove();

    $("[paragraph-text]").contents().filter(function(){
        return this.nodeType === 3  && this.data.trim().length > 0
    }).wrap("<span/>");

    $("[paragraph-text] p:last").each(function(){
        if(!$(this).next().length){
            $(this).css("margin-bottom",0)
        }
    })

    $("[paragraph-text] p:first").each(function(){
        if(!$(this).prev().length){
            $(this).css("margin-top",0)
        }
    })

    /*--- DISMISS CHARACTER BOXES ---*/
    function dismiss_ok(){
        $(".maisnon").on("click", function(dire){
            if(dire.target !== this){
                return
            }

            dismiss_action();
        })

        $(".dis-close").click(function(){
            dismiss_action();
        })
    }

    function dismiss_action(){
        $(".ovcnt").fadeOut(boxInOut);

        setTimeout(function(){
            $(".popup-box").css("height","");
            $("[popup-img-cont], .mt-evst").css("height","")
            $("[popup-img-cont] img").css("height","")
        },boxInOut);
    }
    /*-----------------------------*/

    $("a[title], [menu-character][title]").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:69,
        tip_fade_speed:120,
        attribute:"title"
    });

    /*-----------------------------*/

    if($.browser.mozilla){
        var qiqxo = getComputedStyle(document.documentElement).getPropertyValue("--Scrollbar-Width");
        if(qiqxo == "0" || qiqxo == "0px"){
            $(".mt-evst").css("scrollbar-width","none")
        } else {
            $(".mt-evst").css("scrollbar-width","thin")
        }
    }

    /*-----------------------------*/

    if(customize_page){
        $("body").eq(0).append("<div class='note-to-user'>Hello! Please read <a href=\'//docs.google.com/presentation/d/14EJlSGPCq-xH6YlT1JcOFzk_SEl_z7fl9ozhdKCP31o/edit?usp=sharing\'>this guide</a> to get started!</div>");
        $(".note-to-user").css({
            "position":"fixed",
            "top":"0",
            "left":"0",
            "width":"100%",
            "padding":"18px",
            "background":"#130f1a",
            "box-sizing":"border-box",
            "font-family":"LT emphasis",
            "font-size":"14px",
            "text-transform":"uppercase",
            "letter-spacing":"1.5px",
            "color":"#b4adb8",
            "line-height":"1em",
            "text-align":"center",
            "z-index":"9"
        })

        $(".note-to-user a").css({
            "color":"#f76161",
            "font-weight":"bold",
            "padding":"0",
            "border-bottom":"none"
        })
    }
})//end ready
